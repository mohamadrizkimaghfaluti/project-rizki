package com.example.MiniProjectUniversity.controller;

import com.example.MiniProjectUniversity.dto.ExamRequestDto;
import com.example.MiniProjectUniversity.dto.ExamResponseDto;
import com.example.MiniProjectUniversity.model.Exam;
import com.example.MiniProjectUniversity.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("exam")
public class ExamController {

    @Autowired
    private ExamService service;

    @GetMapping
    public ResponseEntity<List<Exam>> findAll(){
        List<Exam> examList = service.findAll();
        return new ResponseEntity<>(examList, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Exam> create(@RequestBody Exam exam){
        Exam exam1 = service.create(exam);
        return new ResponseEntity<>(exam1, HttpStatus.CREATED);
    }


    @PostMapping("/create-v2")
    public ResponseEntity<Exam> createV2(@RequestBody ExamRequestDto examRequestDto){
        Exam examResponseDto = service.createV2(examRequestDto);
        return new ResponseEntity<>(examResponseDto, HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<ExamResponseDto>> findAllExam(){
        List<ExamResponseDto> examResponseDtos = service.findAllExam();
        return new ResponseEntity<>(examResponseDtos, HttpStatus.OK);
    }

}
