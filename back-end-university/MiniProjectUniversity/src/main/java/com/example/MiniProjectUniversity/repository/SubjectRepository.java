package com.example.MiniProjectUniversity.repository;

import com.example.MiniProjectUniversity.dto.SubjectDto;
import com.example.MiniProjectUniversity.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, String> {

    @Transactional
    @Query("SELECT new com.example.MiniProjectUniversity.dto.SubjectDto(s.codeSubject, s.nameSubject, s.sks, s.semester, st.fullName, ex.examLevel, e.score) FROM Subject s, Student st, ExamLevel ex, Exam e WHERE s.codeSubject=:codeSubject")
    public List<SubjectDto> projection(String codeSubject);

}
