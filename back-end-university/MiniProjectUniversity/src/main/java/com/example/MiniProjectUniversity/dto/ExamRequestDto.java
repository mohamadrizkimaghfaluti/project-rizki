package com.example.MiniProjectUniversity.dto;

import java.util.Date;

public class ExamRequestDto {
    private String examCode;
    private String idStudent;
    private String idLecturer;
    private String codeSubject;
    private String examLevelCode;
    private int score;
    private String date;

    public String getExamCode() {
        return examCode;
    }

    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }

    public String getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(String idStudent) {
        this.idStudent = idStudent;
    }

    public String getIdLecturer() {
        return idLecturer;
    }

    public void setIdLecturer(String idLecturer) {
        this.idLecturer = idLecturer;
    }

    public String getCodeSubject() {
        return codeSubject;
    }

    public void setCodeSubject(String codeSubject) {
        this.codeSubject = codeSubject;
    }

    public String getExamLevelCode() {
        return examLevelCode;
    }

    public void setExamLevelCode(String examLevelCode) {
        this.examLevelCode = examLevelCode;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
