package com.example.MiniProjectUniversity.dto;

import com.sun.istack.NotNull;

import javax.persistence.Column;

public class SubjectDto {

    private String codeSubject;
    private String nameSubject;
    private int sks;
    private int semester;
    private String fullName;
    private String examLevel;
    private int score;

    public SubjectDto() {
    }

    public SubjectDto(String codeSubject, String nameSubject, int sks, int semester, String fullName, String examLevel, int score) {
        this.codeSubject = codeSubject;
        this.nameSubject = nameSubject;
        this.sks = sks;
        this.semester = semester;
        this.fullName = fullName;
        this.examLevel = examLevel;
        this.score = score;
    }

    public String getCodeSubject() {
        return codeSubject;
    }

    public void setCodeSubject(String codeSubject) {
        this.codeSubject = codeSubject;
    }

    public String getNameSubject() {
        return nameSubject;
    }

    public void setNameSubject(String nameSubject) {
        this.nameSubject = nameSubject;
    }

    public Integer getSks() {
        return sks;
    }

    public void setSks(Integer sks) {
        this.sks = sks;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getExamLevel() {
        return examLevel;
    }

    public void setExamLevel(String examLevel) {
        this.examLevel = examLevel;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
