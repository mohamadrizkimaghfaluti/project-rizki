package com.example.MiniProjectUniversity.service;

import com.example.MiniProjectUniversity.dto.ExamRequestDto;
import com.example.MiniProjectUniversity.dto.ExamResponseDto;
import com.example.MiniProjectUniversity.model.*;
import com.example.MiniProjectUniversity.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class ExamService {

    @Autowired
    private ExamRepository repository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private LecturerRepository lecturerRepository;

    @Autowired
    private ExamLevelRepository examLevelRepository;

    @Autowired
    private ExamListRepository examListRepository;

    public List<Exam> findAll(){
        List<Exam> examList = repository.findAllExam();
        return examList;
    }

    public List<ExamResponseDto> findAllExam(){
        List<ExamResponseDto> examResponseDtos = examListRepository.findAllExam();
        return examResponseDtos;
    }

    @Transactional(rollbackFor = Throwable.class)
    public Exam create(Exam exam){
        Exam dataExam = insertDataExam(exam);
        repository.save(exam);
        return dataExam;
    }

    @Transactional(rollbackFor = Throwable.class)
    public Exam createV2(ExamRequestDto examRequestDto){
        Exam requestDto = new Exam();
        requestDto.setExamCode(examRequestDto.getExamCode());
        requestDto.setStudent(studentRepository.findById(examRequestDto.getIdStudent()).get());
        requestDto.setLecturer(lecturerRepository.findById(examRequestDto.getIdLecturer()).get());
        requestDto.setSubject(subjectRepository.findById(examRequestDto.getCodeSubject()).get());
        requestDto.setExamLevel(examLevelRepository.findById(examRequestDto.getExamLevelCode()).get());
        requestDto.setScore(examRequestDto.getScore());
        requestDto.setDate(examRequestDto.getDate());
        return repository.save(requestDto);
    }

    private Exam insertDataExam(Exam exam){
        Exam exam1 = new Exam();
        if (exam.getExamCode().equals("") || exam.getExamCode()==null){
            exam1.setExamCode(String.valueOf(randomNumber()));
            exam1.setScore(exam.getScore());
            exam1.setDate(date());

            Subject subject = checkIdSubject(exam);
            exam1.setSubject(subject);


            Student student = checkIdStudent(exam);
            exam1.setStudent(student);

            Lecturer lecturer = checkIdLecturer(exam);
            exam1.setLecturer(lecturer);

            ExamLevel examLevel = checkExamLevelCode(exam);
            exam1.setExamLevel(examLevel);
        }
        return exam1;
    }

    private ExamLevel checkExamLevelCode(Exam exam) {
        ExamLevel examLevel = null;
        List<ExamLevel> examLevels = examLevelRepository.findAll();
        for (ExamLevel level : examLevels){
            if (level.getExamLevelCode().equals(exam.getExamLevel())){
                examLevel = exam.getExamLevel();
            }
        }
        return examLevel;
    }

    private Lecturer checkIdLecturer(Exam exam) {
        Lecturer lecturer = null;
        List<Lecturer> list = lecturerRepository.findAll();
        for (Lecturer lecturer1: list){
            if (lecturer1.getIdLecturer().equals(exam.getLecturer())){
                lecturer = exam.getLecturer();
            }
        }
        return lecturer;
    }

    private Student checkIdStudent(Exam exam) {
        Student student = null;
        List<Student> students = studentRepository.findAll();
        for (Student student1 : students){
            if (student1.getIdStudent().equals(exam.getStudent())){
                student = exam.getStudent();
            }
        }
        return student;
    }

    private Subject checkIdSubject(Exam exam) {
        Subject subject = null;
        List<Subject> subjects = subjectRepository.findAll();
        for (Subject subject1:subjects){
            if (subject1.getCodeSubject().equals(exam.getSubject())){
                subject = exam.getSubject();
            }
        }
        return subject;
    }

    private int randomNumber() {
        Random rnd = new Random();
        int number = rnd.nextInt(99999999);
        int k = Integer.parseInt(String.format("%08d", number));
        return k;
    }

    private String date(){
        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
